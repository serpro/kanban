
Objetivo: Tornar a solução mais modularizável

Permitir que pessoas com expertize diferentes possam efetuar contribuições


1 - Torna essa aplicação em apenas uma Aplicação API utilizando RAILS

 - Integração Gitlab
 - Resposta à erros padronizadas
 - WebSocket fazendo parte da api (um endpoint específico para isso)
 - API para autenticação
 - Melhoras na API
    - Utilizar Presenters => http://blog.arkency.com/2013/01/rails-api-my-approach/
    - Utilizar verbos HTTP corretos
    - Paginação

2 - Mover aplicação angular para uma aplicação Frontend independente

Objetivo: Mais pessoas poderão colaborar com melhorias apenas de interface

 - Javascript e CSS ficam nessa aplicação frontend
 - Navegação e templates também ficam na aplicação Angular
 - Reorganizar a aplicação Angular (mais serviços, tratamento de erros, responsive)

3 - Mover para Banco Postgresql

 - Facilitar que mais pessoas possam colaborar
 - Facilitar infraestrutura

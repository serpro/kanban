class ApplicationController < ActionController::Base

  before_filter :authenticate

  helper_method :current_user
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout 'layout'

  after_filter :set_csrf_cookie_for_ng

protected
  def current_user= value
    if value
      session[:user_id] = value[:_id]
    else
      session[:user_id] = nil
    end
  end

  def current_user
    begin
      user = User.find(session[:user_id])
    rescue
      return nil
    end
    user
  end

  def authenticate
    redirect_to '/auth/ldap/' unless current_user
  end

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  def verified_request?
    super || form_authenticity_token == request.headers['X-XSRF-TOKEN']
  end

end

class KanbansController < ApplicationController
  def index
    project = Project.find_by(name: params[:project_id])
    if(project)
      render :json => project.kanbans
    else
      error 404, 'project not found'
    end
  end

  def template
    if(Project.default_template)
      Project.default_template.kanbans[0].to_json(:except => ["_id", "tasks"], :include => {:status => {:include => :tasks}})
    else
      {}.to_json
    end
  end

  def show
    project = Project.find_by(name: params[:project_id])
    if(project)
      kanban = project.kanbans.find(params[:id])
      render :json => kanban.to_json(:exclude => :tasks, :include => {:status => {:include => :tasks}, :status_deleted => true })
    else
      error 404, 'project not found'
    end
  end

  def update
    project = Project.find_by(name: params[:project_id])
    if(project)
      kanban_sent = JSON.parse(request.body.read.to_s)
      kanban = project.kanbans.find(params[:id])
      return error 404, 'kanban not found' unless kanban
      if(params[:custom_action])
        result = kanban.send(params[:custom_action], kanban_sent)
      else
        if kanban.update_attributes kanban_sent
          result = kanban
        else
          return render :json => {errors: kanban.errors.full_messages.join }, status: 422
        end
      end
      if result.is_a? Kanban
	      render :json => result
      else
        render :json => result.as_json
      end
    else
      error 404, 'project not found'
    end
  end

  def create
    project = Project.find_by(name: params[:project_id])
    if(project)
      kanban_sent = JSON.parse(request.body.read.to_s)
      render :json => project.create_kanban(kanban_sent)
    else
      error 404, 'project not found'
    end
  end
end

class ProjectsController < ApplicationController
  def index
    projects = Project.all.order_by(:name => 'asc').sort.reject {|p| p.name == 'default_template'}
    render :json => projects.to_json({:only => [:_id, :name, :status_set]})
  end

  def dashboard
    @publics = Project.public
    #TODO only return private projects current_user has access to
    @privates = Project.private
  end

  def create
    project_sent = JSON.parse(request.body.read.to_s)
    project = Project.create(project_sent)
    render :json => project.to_json
  end

  def show
    project = Project.find_by(:name => params[:id])
    render :json => project
  end


  def update
    project_sent = JSON.parse(request.body.read.to_s)
    project = Project.find_by(name: project_sent.delete("id"))

    if(project)
      project_sent['status_set'] = Project.transform_status_set(project_sent['status_set'])
      project.update_attributes(project_sent)
      render :text => 'project sucessfully updated'
    else
      error 404, 'project not found'
    end
  end

  #delete project by name
  def destroy
    project = Project.where(name: params[:name])
    if(project)
      project.destroy
      render :text => 'project successfully deleted'
    else
      error 404, 'project not found'
    end
  end
end

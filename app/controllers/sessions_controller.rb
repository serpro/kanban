class SessionsController < ApplicationController

  protect_from_forgery :except => :create
  before_filter :authenticate, :except => [:login, :new, :create, :failure]

  layout 'session'

  def create
    @user = User.find_or_create_from_auth_hash(auth_hash)
    self.current_user = @user
    respond_to do |format|
      format.js
      format.html do
        redirect_to root_url
      end
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Signed out!"
  end

  def failure
    flash[:error] = "Authentication_Failed"
    render :login
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end

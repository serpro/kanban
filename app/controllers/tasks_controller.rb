class TasksController < ApplicationController
  def create
    project = Project.find_by(name: params[:project_id])
    if(project)
      kanban = project.kanbans.find(params[:kanban_id])
      if(kanban)
        task = JSON.parse(request.body.read.to_s)
        new_task = kanban.tasks.create(task)
        render :json => new_task.to_json
      else
        error 404, 'kanban not found'
      end

    else
      error 404, 'project not found'
    end
  end

  def destroy
    project = Project.find_by(name: params[:project_id])
    if(project && kanban=project.kanbans.find(params[:kanban_id]))
        kanban.tasks.find(params[:id]).destroy
        render :json => {result: 'ok'}.to_json
    else
      error 404, 'project/kanban not found'
    end
  end

  def update
    project = Project.find_by(name: params[:project_id])
    if(project && kanban=project.kanbans.find(params[:kanban_id]))
      data = JSON.parse(request.body.read.to_s)

      unless(params[:move])
        Hash[data.map {|k,v| [k.to_sym, v] }]
        task = kanban.tasks.find(params[:id])
        task.update_attributes(data)
      else
        status_to= kanban.status.find(data["status_id"])
        task = kanban.tasks.find(data["id"])
        position_to = data["position"]
        kanban.move_task(task, status_to, position_to)
      end

      render :json => {result: 'ok'}.to_json
    else
      error 404, 'project/kanban not found'
    end
  end
end

class UploadController < ApplicationController


  def index
    upload = current_user.store_upload(params)
    content = File.open(upload.filepath,'r').read
    render :json => {
              :id => upload.to_param,
              :filename => upload.filename,
              :content => content
            }
  end

protected
  def verified_request?
    true
  end
end

class Activity
  include Mongoid::Document

  belongs_to :user
  belongs_to :task
end

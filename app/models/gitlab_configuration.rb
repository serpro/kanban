class GitlabConfiguration
  include Mongoid::Document
  
  field :server, type: String
  field :private_token, type: String
  field :project_id, type:String

  validates_presence_of :server_url
  validates_presence_of :private_token
  validates_presence_of :project_id

  embedded_in :project
end

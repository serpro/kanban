
class GitlabService
  INVALID_SERVER_OR_TOKEN = 'Invalid Gitlab server configuration or private token'
  NO_PROJECT_DEFINED = 'No current project defined'

  def initialize(options={})
    service_options = {
      gitlab_server: '',
      private_token: ''
    }
    service_options.merge! options
    @gitlab_server = service_options[:gitlab_server]
    @private_token = service_options[:private_token]

    @user = nil
    @gitlab_cli = Gitlab.client(endpoint: @gitlab_server, private_token: @private_token)
    validate_server
    validate_token
  end


  def is_token_valid?
    @valid_token
  end


  def get_projects
      @gitlab_cli.projects(per_page: 30)
  end

  def get_project(id)
    @gitlab_cli.project id
  end

  def get_issues(project_id=nil, milestone = nil, state = nil)
    @gitlab_cli.issues(project_id)
  end

  def create_issue(project_id, issue_params={})
    title = issue_params.delete(:title)
    @gitlab_cli.create_issue(project_id, title, issue_params)
  end

  def close_issue(project_id, issue_id)
    @gitlab_cli.close_issue project_id, issue_id
  end

  def move_issue_to_milestone(issue_id, target_milestone)
    options = {
      milestone_id: target_milestone
    }
    @gitlab_cli.edit_issue current_project_id, issue_id, options
  end


  def update_issue(issue_id, title=nil, description=nil, labels=nil)
    options = {}
    if title or description or labels
      options[:title] = title if title
      options[:description] = description if description
      options[:labels] = labels if labels
      @gitlab_cli.edit_issue current_project_id, issue_id, options
    end
  end

  def currrent_user
    @user
  end

private

  def validate_server
    if @gitlab_server.blank?
      raise GitlabServiceException.new(INVALID_SERVER_OR_TOKEN)
    end
  end

  def validate_token
    @user ||= @gitlab_cli.user
    @valid_token = true
  rescue Gitlab::Error::MissingCredentials => e
    @valid_token = false
    raise GitlabServiceException.new(INVALID_SERVER_OR_TOKEN)
  end
end

require 'csv'
class Kanban
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  #Mongoid::Errors::UnknownAttribute:
  #  Problem:
  #    Attempted to set a value for 'fromtime(1i)' which is not allowed on the model Event.
  #  Summary:
  #    Without including Mongoid::Attributes::Dynamic in your model and the attribute does not already exist in the attributes hash, attempting to call Event#fromtime(1i)= for it is not allowed. This is also triggered by passing the attribute to any method that accepts an attributes hash, and is raised instead of getting a NoMethodError.
  #  Resolution:
  #    You can include Mongoid::Attributes::Dynamic if you expect to be writing values for undefined fields often.

  field :visibility, type: String, default: 'public'
  field :name, type: String
  embedded_in :project
  embeds_many :status
  has_many :tasks

  validates_uniqueness_of :name
  validates_presence_of :name

  validate :check_first_task_field_name
  validate :check_tasks_fields_names
  
  def owner
    project.owner
  end

  def to_json(options={})
    super(options.merge({:id => view_id, :include => :tasks}))
  end

  def view_id
    id.to_s
  end

  def change_status_name(status)
    status_to_change = self.status.find status['_id']
    status_to_change.name = status['name']
    status_to_change.save!
  end

  def add_status(params_hash)
    new_status = status.new(name: params_hash['new_status'])
    save!
    { :_id => self._id, :new_status => new_status }
  end

  def move_task(task, status_to, position_to)
    return if task.kanban_id != self.id   #only move tasks from this kanban
    return if status_to.kanban.id != self.id #only if the status_to is within this kanban
    #remove task from original status group
    status_from = status.find task.status_id
    status_from.tasks.delete task
    #assign new task sequential
    task.seq = position_to
    #push task to target status group
    status_to.tasks.push task
  end



  def delete_status params_hash
    status_id = params_hash['status_deleted']
    if attributes['status_deleted']
      add_to_set(:status_deleted => status_id) unless  attributes['status_deleted'].include? status_id
    else
      update_attributes!(:status_deleted => [status_id])
    end
    reload
    { :_id => self._id, :status_deleted => attributes['status_deleted'] }
  end

  def restore_status params_hash
    status_id = params_hash["status_deleted"]
    pull(:status_deleted => status_id)
    { :_id => self._id, :status_deleted => attributes['status_deleted'] }
  end

  def batch_load(params_hash)
    upload = Upload.find(params_hash['upload_id'])
    load_tasks_from_csv(upload.filepath)
  end

  def build_burndown(end_date=nil)
    start_date = Date.parse self.scrum_config['start_date']
    end_date = end_date || Date.parse(self.scrum_config['end_date'])
    burndown_serie = []
    points_delivered = 0

    total_points = total_points_estimated

    (start_date..end_date).each_with_index do |date, index|
      if index == 0
        burndown_serie << total_points
      else
        points_delivered += points_delivered_at(date)
        burndown_serie << total_points - points_delivered
      end
    end
    burndown_serie
  end

  def total_points_estimated
    if scrum_config && scrum_config['task_estimation_field']
      estimation_field_name = scrum_config['task_estimation_field']
      total_points_estimated = 0
      tasks.each do |task|
        total_points_estimated += task[estimation_field_name].to_i
      end
      total_points_estimated
    end
  end

  def points_delivered_at date
    tasks.where('delivery_date' => date.strftime('%d/%m/%Y')).sum(:points)
  end

  def points_delivered_until end_date
     start_date = Date.parse self.scrum_config['start_date']
     points = 0
     (start_date..end_date).each do |date|
      points += tasks.where('delivery_date' => date.strftime('%d/%m/%Y')).sum(:points)
     end
     points
  end

  def points_remaining_at date
    total_points_estimated - points_delivered_until(date)
  end

  def load_tasks_from_csv(filename)
      CSV.foreach(filename) do |row|
        self.tasks.create!(:status => status.first, :text => row[0], :collapsed => true)
      end
  end

#VALIDATIONS
  # validation to keep first kanban field name unchanged as 'text'
  def check_first_task_field_name
    return true unless attributes['tasks_fields'].present?
    if attributes['tasks_fields'].length > 0 && ! "text".eql?(attributes['tasks_fields'][0][:name])
      errors.add('tasks_fields', 'Field text can not be changed')
    end
  end
 
 def check_tasks_fields_names
   return if attributes['tasks_fields'].nil?
   return if attributes['tasks_fields'].empty?
   attributes['tasks_fields'].each do |field|
     if /^[a-z_][a-zA-Z_0-9]*$/.match(field[:name]).nil?
       errors.add('tasks_fields', "Field name #{field[:name]} not allowed")
     end
   end
 end
end

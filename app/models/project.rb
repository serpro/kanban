class Project
  include Mongoid::Document
  field :name, type: String
  field :type, type: String, :default => 'project'
  field :status_set, type: String
  field :visibility, type: String, default: 'public'
  field :gitlab_project_id, type: String
  field :gitlab_private_token, type: String
  field :gitlab_server, type: String

  belongs_to :owner, :class_name => 'User'

  embeds_many :kanbans
  embeds_one :gitlab_configuration

  has_and_belongs_to_many :team_members, class_name: 'User'#, :inverse_of => nil

  validates_uniqueness_of :name
  validates_presence_of :name

  default_scope -> { excludes(type: "template") }

  scope :public, -> { where(visibility: 'public') }
  scope :private, -> { where(visibility: 'private') }

  def self.default_template
    self.find_by(name: 'default_template', type: "template")
  end

  def self.templates
    self.where(type: 'template')
  end

  def create_kanban(kanban)
    novo_kanban = self.kanbans.new(name: kanban["name"], 'tasks_fields' => kanban["tasks_fields"], 'scrum_config' => kanban["scrum_config"])
    unless (kanban["status"])
      if(status_set.blank?)
        Status.default_status_set.each do |status_name|
          novo_kanban.status.new(name: status_name)
        end
      else
        statuses = status_set.split(',').map {|x| x.strip }
        statuses.each do |status_name|
          novo_kanban.status.new(name: status_name)
        end
      end
    end
    self.save!
    novo_kanban
  end

  def self.transform_status_set(status_set)
    if status_set.is_a? Array
      status_set.join ','
    else
      status_set.to_s
    end
  end

  def allow_access_to(user)
    return false if user.nil?
    return true if "public".eql?(self.visibility)
    (self.owner and self.owner.id.eql?(user.id)) or self.team_members.include?(user)
  end
end

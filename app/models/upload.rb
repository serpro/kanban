class Upload
  include Mongoid::Document

  belongs_to :user

  field :filename, type: String
  field :filepath, type: String


end

#encoding: utf-8
class User
  include Mongoid::Document
  field :email, type: String
  field :login, type: String
  field :name, type: String
  field :uid, type: String
  field :provider, type: String

  field :gitlab_private_token, type: String

  has_and_belongs_to_many :projects

  has_many :uploads

  #has_many :kanbans

  validates_presence_of :login, :name
  validates_uniqueness_of :login

  def self.signup_if_new(user_data)
    user = find_by(uid: user_data[:uid])
    unless user
      user = User.create(user_data)
    end
    user
  end

  def self.find_or_create_from_auth_hash(auth_hash)
    # auth_hash.info.
    # => description =  abner.oliveira
    # => name =  Abner Silva de Oliveira
    # => email       =  abner.oliveira@serpro.gov.br
    # => nickname    =  80129498572
    # => phone       = 2102-1405
    # => mobile
    # auth_hash.extra.raw_info.ou[0] => #SUPDE/DESDR/DE5CT
    #puts auth_hash.pretty_inspect
    if auth_hash.provider.eql? 'noosfero_oauth2'
      uid = auth_hash.uid
      login = auth_hash.uid
    elsif  auth_hash.provider.eql? 'facebook'
      uid = auth_hash.uid
      login = auth_hash[:info].first_name
    elsif auth_hash.provider.eql? 'gitlab'
      uid = auth_hash.uid
      login = auth_hash[:info].username
    else
      uid = auth_hash.info.nickname
      login = auth_hash.info.description
    end

    user = User.find_or_initialize_by(:uid => uid)

    #raise auth_hash.inspect
    if(user.new_record?)
      user.provider = auth_hash.provider
      user.name = auth_hash.info.name

      user.login = login
      if auth_hash.provider.eql? 'gitlab'
        user.gitlab_private_token = auth_hash[:extra][:raw_info].private_token
      end
      user.uid = uid
      user.email = auth_hash.info.email
      user.save!
    end
    user
  end


  def store_upload(upload_params)
    return {:error => 'Formato inválido' } unless validate_upload_type(upload_params['upload'])

    original_filename =  upload_params['upload'].original_filename
    filename = sanitize_filename(original_filename)

    directory = "public/upload"
    # create the file path
    filename_user_prefixed = "#{self.login}_#{filename}"
    path = File.join(directory, filename_user_prefixed)
    File.open(path, "wb") { |f| f.write(upload_params['upload'].read) }
    self.uploads.create!(:filename => original_filename, :filepath => path)
  end
private
  def validate_upload_type(upload_file)
    ['text/csv'].include? upload_file.content_type
  end
  def sanitize_filename(file_name)
    # get only the filename, not the whole path (from IE)
    just_filename = File.basename(file_name)
    # replace all none alphanumeric, underscore or perioids
    # with underscore
    just_filename.sub(/[^\w\.\-]/,'_')
  end

end

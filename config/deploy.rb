require "rvm/capistrano"
require 'bundler/capistrano'

set :application, "kanban.serpro"

#set :repository, 'https://github.com/abner/kanban-angularjs-sinatra.git'
#set :repository,  "git@github.com:abner/kanban-angularjs-sinatra.git"
#set :repository, 'https://gitlab.com/serpro/kanban.git'
set :repository, 'http://gitlab.serpro/desdr/kanban-serpro.git'

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "kanban.serpro.novo"                          # Your HTTP server, Apache/etc
role :app, "kanban.serpro.novo"                          # This may be the same as your `Web` server
#role :db,  "your primary db-server here", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

set :rvm_type, :system

set :rvm_ruby_string, 'ruby-1.9.3-p547'

set :linked_dirs, %w{bin log tmp/backup tmp/pids tmp/cache tmp/sockets vendor/assets vendor/bundle}
set :linked_files, 'kanban.service'

set :user, "kanban"

set :bundle_flags, "--no-deployment"

set :use_sudo, false

set :deploy_to, "/var/aplic/kanban"
# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

def run_remote_rake(rake_cmd)
  rake_args = ENV['RAKE_ARGS'].to_s.split(',')
  cmd = "cd #{fetch(:latest_release)} && bundle exec #{fetch(:rake, "rake")} RAILS_ENV=#{fetch(:rails_env, "production")} #{rake_cmd}"
  cmd += "['#{rake_args.join("','")}']" unless rake_args.empty?
  run cmd
  set :rakefile, nil if exists?(:rakefile)
end

# If you are using Passenger mod_rails uncomment this:
 namespace :deploy do
   task :start do
       run "systemctl enable /var/aplic/kanban/shared/kanban.service"
       run "systemctl stop /var/aplic/kanban/shared/kanban.service"
       run "systemctl start /var/aplic/kanban/shared/kanban.service"
   end
   task :stop do ; end
   
   
   task :restart, :roles => :app, :except => { :no_release => true } do
     #run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
     #run_remote_rake 'deploy:restart_web_servers'

     #puts "Waiting server to start"
     run "systemctl enable /var/aplic/kanban/shared/kanban.service"
     run "systemctl stop /var/aplic/kanban/shared/kanban.service"
     run "systemctl start /var/aplic/kanban/shared/kanban.service"
     
     sleep(30)
     run "ps -aux  | grep thin"
   end
 end

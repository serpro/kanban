require 'omniauth/strategies/noosfero_oauth2'

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :ldap, :title => 'Serpro LDAP',
    :host => ENV['KANBAN_LDAP_HOST'],
    :port => ENV['KANBAN_LDAP_PORT'],
    :method => :plain,
    :base => ENV['KANBAN_LDAP_BASE'],
    :uid => ENV['KANBAN_LDAP_UID'],
    :password => ENV['KANBAN_LDAP_PASSWORD'],
    :try_sasl => true.to_s.eql?(ENV['KANBAN_LDAP_TRY_SASL']),
    :bind_dn => ENV['KANBAN_LDAP_BIND_DN'],
    :form => SessionsController.action(:login)

  #provider :noosfero_oauth2,
  #    ENV['NOOSFERO_KEY'], ENV['NOOSFERO_SECRET'],
  #  :client_options => { :site => ENV['NOOSFERO_URL'] }

  if 'production'.eql?(Rails.env)
    provider :facebook, ENV['FACEBOOK_KEY'], ENV['FACEBOOK_SECRET']
  else
    provider :facebook, '852008221508339', '2a78817d2ae61811e16069fe9b7ed0fa',
      scope: 'email'
  end

  provider :gitlab, 'c573f5b8570ca2c8db3ae900abb5c690c28ff2705bd2ddb069b0be6837fd8951', '71288c36f625b0a9eb8cb2009140605eb4eabfd4522b005d13dabae1f6d19b16'

end



OmniAuth.config.on_failure = Proc.new { |env|
  OmniAuth::FailureEndpoint.new(env).redirect_to_failure
}

OmniAuth.config.logger = Rails.logger

# Rake task to restart web servers
# this task only kills the thin instances
# Monit will be watching for the process and will recreate them
# Monit script 'kanban_monit' avaiable at project root folder
KANBAN_HTTP_PORTS = [3838,3839, 3840]
KANBAN_PIDS_PATH = "/rede/aplic/kanban/shared/pids/"

def read_pids
  pids = []
  KANBAN_HTTP_PORTS.each do |http_port|
    File.open("#{KANBAN_PIDS_PATH}thin.#{http_port}.pid", 'r') do |f|
      pids << f.read
    end
  end
  pids
end

namespace :deploy do
  desc "Restart web servers"
  task :restart_web_servers do
    read_pids.each do |pid|
      #only kills the thin instances
      `kill -15 #{pid}`
    end
  end
end

angular.module('ng').directive('ngFocus', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      scope.$watch(attrs.ngFocus, function(val) {
        if (angular.isDefined(val) && val) {
          $timeout(function() {
            element[0].focus();
          });
        }
      }, true);

      element.bind('blur', function() {
        if (angular.isDefined(attrs.ngFocusLost)) {
          scope.$apply(attrs.ngFocusLost);

        }
      });
    }
  };
});

myApp.directive('sameHeight', function ($window, $timeout) {
        var sameHeight = {
            restrict: 'A',
            groups: {},
            link: function (scope, element, attrs) {
                $timeout(getHighest, 0); // make sure angular has proceeded the binding
                angular.element($window).bind('resize', getHighest);
                scope.$watch(attrs.sameHeightWatcher, function(){
                   $timeout(getHighest, 100);
                });
                function getHighest() {
                    if (!sameHeight.groups[attrs.sameHeight]) { // if not exists then create the group
                        sameHeight.groups[attrs.sameHeight] = {
                            height: 0,
                            elems:[]
                        };
                    }
                    sameHeight.groups[attrs.sameHeight].elems.push(element);
                    element.css('height', ''); // make sure we capture the origin height

                    if (sameHeight.groups[attrs.sameHeight].height < element.outerHeight()) {
                        sameHeight.groups[attrs.sameHeight].height = element.outerHeight();
                    }

                    //console.log(sameHeight.groups[attrs.sameHeight].height);

                    if(scope.$last){ // reinit the max height
                       angular.forEach(sameHeight.groups[attrs.sameHeight].elems, function(elem){
                            var offSetHeight = parseInt(elem.attr('same-height-offset'));
                            offSetHeight = (offSetHeight == null || offSetHeight == 'undefined') ? 0 : offSetHeight;
                            //console.log("OFFSETHEIGHT: ", offSetHeight);
                            //console.log("CALCULATED: ", sameHeight.groups[attrs.sameHeight].height + offSetHeight);
                            elem.css('height', sameHeight.groups[attrs.sameHeight].height + offSetHeight);

                        });
                        //sameHeight.groups[attrs.sameHeight].height = 0;
                    }
                }
            }
        };
        return sameHeight;
    });
/*
SOLUTION IF WE ARE GOING TO USE DOUBLECLICK TO ACTIVATE CONTENTEDITABLE
http://stackoverflow.com/questions/19815403/setting-contenteditable-on-double-click-ie-does-not-allow-editing-unless-click */
myApp.directive('contenteditable', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ctrl) {
      // view -> model
      element.bind('blur', function() {
        scope.$apply(function() {
          ctrl.$setViewValue(element.html()
                .replace(/<br(\s*)\/*>/ig, '\n') // replace single line-breaks
                .replace(/<br(\s*)>/ig, '\n') // replace single line-breaks
                .replace(/<[p|div]\s/ig, '\n$0') // add a line break before all div and p tags
                .replace(/(<([^>]+)>)/ig, "") );
        });
        if (attrs.contenteditableAction){
            scope.$apply(attrs.contenteditableAction);
        }
      });

      // model -> view
      ctrl.$render = function() {
        element.html(ctrl.$viewValue);
      };

      // load init value from DOM
      //ctrl.$setViewValue(element.html());
      ctrl.$render();
    }
  };
});

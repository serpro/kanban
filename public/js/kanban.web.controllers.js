myApp.controller('GitlabProjectsCtrl', function($scope, $rootScope, $route, $routeParams, $location, $q, gitlabService){

	$scope.gitlabProviders = [
		{
			id: 'gitlab.serpro',
			name: 'Gitlab.serpro',
			api_host: 'http://gitlab.serpro/api/v3'
		},
		{
			id: 'gitlab.local',
			name: 'Gitlab Localhost 3001',
			api_host: 'https://localhost:3001/api/v3'
		},
		{
			id: 'gitlab.com',
			name: 'Gitlab.com',
			api_host: 'https://gitlab.com/api/v3'
		}
	];

	$scope.gitlabProjects = [];

	$scope.gitlab_phase = 'select_provider';

	$scope.gitlabProviderChanged = function(){
		$rootScope.gitlabProvider = $scope.gitlab_provider;

		console.log($scope.gitlab_provider);

		gitlabService.configure($scope.gitlab_provider.api_host, '');
	};

	$scope.getGitLabProjectNames = function(){
      gitlabService.getProjects().then(function(data){
        $scope.gitlabProjects = [];
        $scope.gitlabProjects = data;
      });
	};

	$scope.getGitLabLabels = function() {
		gitlabService.getLabels().then(function(data){
			$scope.gitLabProject.labels = data;
		});
	}

	$scope.loginGitLab = function() {
		var login = $scope.gitlab_login;
		var password = $scope.gitlab_password;

		gitlabService.login(login, password).then(function(user){
			$scope.gitlab_phase = 'select_project';
			$scope.getGitLabProjectNames();
		},function(data){
			alert(data);
			alert('Autenticação falhou: ' + data.message)
		});
	};

	$scope.gitlabSelectProject = function() {
		gitlabService.getMilestones($scope.gitlab_project.id).then(function(milestones){
			$scope.gitlab_phase = 'select_milestone';
			$scope.gitlabMilestones = milestones;
			console.log(milestones);
		});
		gitlabService.getLabels($scope.gitlab_project.id).then(function(labels){
			console.log(labels);
			$scope.gitlab_project.labels = labels;
		});
	};

	$scope.gitlabSelectMilestone = function() {
		console.log($scope.gitlab_milestone);
	}

	$scope.gitlabMapColumnToLabel = function() {
		//maps a kanban column to a label
	}

	$scope.addLabel = function() {
		//adds label to gitlab
	}
});

var Kanban = {};

Kanban.HashTable = function(obj)
{
    this.length = 0;
    this.items = {};
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            this.items[p] = obj[p];
            this.length++;
        }
    }

    this.setItem = function(key, value)
    {
        var previous = undefined;
        if (this.hasItem(key)) {
            previous = this.items[key];
        }
        else {
            this.length++;
        }
        this.items[key] = (value == null || value == 'undefined') ? key : value;
        return previous;
    }

    this.getItem = function(key) {
        return this.hasItem(key) ? this.items[key] : undefined;
    }

    this.hasItem = function(key)
    {
        return this.items.hasOwnProperty(key);
    }

    this.removeItem = function(key)
    {
        if (this.hasItem(key)) {
            previous = this.items[key];
            this.length--;
            delete this.items[key];
            return previous;
        }
        else {
            return undefined;
        }
    }

    this.keys = function()
    {
        var keys = [];
        for (var k in this.items) {
            if (this.hasItem(k)) {
                keys.push(k);
            }
        }
        return keys;
    }

    this.values = function()
    {
        var values = [];
        for (var k in this.items) {
            if (this.hasItem(k)) {
                values.push(this.items[k]);
            }
        }
        return values;
    }

    this.each = function(fn) {
        for (var k in this.items) {
            if (this.hasItem(k)) {
                fn(k, this.items[k]);
            }
        }
    }

    this.clear = function()
    {
        this.items = {}
        this.length = 0;
    }
}

myApp.controller('ProjectSelectionCtrl', function($scope, $rootScope, webServiceStorage, $route, $routeParams, $location, $q) {
	$rootScope.localStorageEnabled = false;
	$rootScope.dropboxServiceEnabled = false;

	$scope.$route = $route;
	$scope.$routeParams = $routeParams;

	$scope.currentProvider = $routeParams.provider_name;

	$scope.currentProject = $routeParams.project_name;
	$scope.showProjetoSelection = true;

	$scope.projects = webServiceStorage.Project.query(function(data) {
		for (i = 0; i < data.length; i++) {
			var value = data[i];
			if (value.name == $routeParams.project_name) {
				$scope.currentProject = value.name;
				$scope.showProjetoSelection = false;
				$scope.kanbans = $rootScope.kanbans = webServiceStorage.Kanban($scope.currentProject).query();
			}
		}
	});

	$scope.showKanbansList = function() {
		return ($rootScope.kanbans != undefined && $rootScope.kanbans.length > 0);
	};


	$scope.kanbans = [];
	$scope.novoProjeto = '';
	$scope.path = function() {
		return $location.path()
	};

	$scope.projectChanged = function() {
		$location.path('projeto/' + $scope.currentProject);
	};

	$scope.kanbanChanged = function() {
		var KanbanResource = webServiceStorage.Kanban($scope.currentProject);

		var kanban = KanbanResource.get({kanbanId: $scope.currentKanban._id}, function(kanban) {
			webServiceStorage.setLastKanban(kanban);
			$location.path('projeto/' + $scope.currentProject + "/" + $scope.currentKanban._id);
		})
	};

	$scope.addNewProject = function() {
		var projeto = new webServiceStorage.Project();
		projeto.name = $scope.novoProjeto;
		$scope.novProjeto = '';
		projeto.$save(function(projeto) {
			$scope.showProjetoSelection = false;
			$location.path('projeto/' + projeto.name);
		});

	}

	$scope.projectSetup = function(){
		$rootScope.currentProject = webServiceStorage.Project.get({projectId: $scope.currentProject }, function(){
			$location.path('projeto/' + $scope.currentProject + '/setup');
		});
	};

	$scope.addNewKanban = function() {
		var KanbanResource = webServiceStorage.Kanban($scope.currentProject);
		var kanban = new KanbanResource();
		kanban.name = $scope.novoKanban;
		kanban.tasks_fields = [
			{name: 'text', label: '', multiline: true },
			{name: 'who', label: 'Quem: ', multiline: false },
			{name: 'points', label: 'Pontos: ', multiline: false },
			{name: 'delivery_date', label: 'Data de Engrega: ', multiline: false, date: true },
			{name: 'acceptance_criteria', label: 'Critérios de Aceite', multiline: true }
		];

		kanban.scrum_config = {
			start_date: null,
			end_date: null,
			task_estimation_field: 'points',
			task_delivery_date_field: 'delivery_date',
			delivery_status_id: null
		};

		kanban.$save(function(kanban) {
			webServiceStorage.setLastKanban(kanban);
			$location.path('projeto/' + $scope.currentProject + '/' + kanban._id);
		});

	}

	$scope.$watch(function() {
		return $location.path()
	}, function() {
		//console.log($location);
		//console.log($scope.$route.current);
		//console.log($scope.$routeParams);
		$scope.projects = webServiceStorage.Project.query();

	});

	$scope.retornar = function() {
		window.history.back();
	}
});

myApp.controller('ProjectSetup', function($scope, $filter, $rootScope, webServiceStorage, $route, $routeParams, $location, $q){


	$scope.projectName = $routeParams.project_name;

	if($rootScope.currentProject == null || $rootScope.currentProject === 'undefined'){
		$rootScope.currentProject = webServiceStorage.Project.get({projectId: $scope.projectName }, function(p){
			if($scope.currentProject.status_set !== undefined){
				$scope.kanban_custom_columns = $scope.currentProject.status_set;
			}
		});

	} else {

	}

	$scope.isNew = function() {
		if($rootScope.currentProject == undefined){
			return '';
		}
		return ($rootScope.currentProject.kanbans == undefined)
	};

	$scope.sortableOptions = {
    connectWith: ".kanban_columns_list"
	};

	var basicTemplate = new Kanban.HashTable({});
	basicTemplate.setItem('Backlog');
	basicTemplate.setItem('Fazendo');
	basicTemplate.setItem('Pronto');

	var template1 = new Kanban.HashTable({});
	template1.setItem('Backlog');
	template1.setItem('Projetando');
	template1.setItem('Desenvolvendo');
	template1.setItem('Testando');
	template1.setItem('Entregue');


	$scope.basic_template =  basicTemplate;

	$scope.templates = [{name: 'Básico', columns: basicTemplate}, {name: 'Template 1', columns: template1}];

	$scope.defineTemplate = function(template){
		$scope.templateSelected = template;
	}


	$scope.selected_column_template = 'custom';

	$scope.addNewColumn = function() {
		$scope.templateSelected.setItem($scope.newColumnText);
	};

	$scope.removeColumn = function(column_name){
		$scope.templateSelected.removeItem(column_name);
	};

	if($scope.currentProject.status_set !== undefined){
		$scope.kanban_custom_columns = $scope.currentProject.status_set;
	}

	$scope.defineProjectStatusColumns = function(){
		if ($scope.selected_column_template == 'custom') {
			$scope.currentProject.status_set = $scope.kanban_custom_columns;
		} else {
			var template = $filter('getBy')('name', $scope.templates, $scope.selected_column_template);
			$scope.currentProject.status_set = template.columns.keys().join();
		}


		var projetoResourceObject = new webServiceStorage.Project();
		projetoResourceObject.id = $scope.currentProject.name;
		projetoResourceObject.status_set = $scope.currentProject.status_set;
		projetoResourceObject.$update(function(){
		 	$location.path('projeto/' + $scope.currentProject.name);
		});
	};
});

myApp.controller('ProjectGitlabSetup', function($scope, $rootScope, webServiceStorage, $route, $routeParams, $locatin, $q){

})

myApp.controller('KanbanCtrl', function($scope, $filter, $routeParams, $rootScope, kanbanRefreshService, $location, localStorageService, webServiceStorage, gitlabService, $q, $parse) {
//	$scope.templateFromWebService = webServiceStorage.KanbanTemplate.get();

	$scope.taskUpdated = function(targetIdentity, moveData) {
		for(var i=0; i < moveData.origin.length ; i++ ){
			var task = moveData.origin[i];
			task.seq = i;
			task.id = task._id;
			var taskData = new $scope.TaskResource(task);

			taskData.$update(function(){
				if(moveData != null && moveData.origin.length > 0) {
					var status_id = moveData.origin[0].status_id;
					kanbanRefreshService.sendMessage({
							type: 'status',
							project_name: $scope.projectName,
							kanban_id: $scope.kanban._id,
							status_id: status_id
					});
				}
			});
		}

		//console.log(moveData.origin);
	};

	$scope.taskMoved = function(targetIdentity, moveData) {
		$scope.taskEventCount +=1;
		//console.log(moveData);
		//console.log(targetIdentity);
		var task = moveData.dest[moveData.destPosition];
		var origIdentity = task.status_id;

		function enviarMsgAtualizacao() {
			var statusOrigem = $filter('getBy')('tasks', $scope.kanban.status, moveData.origin);
			var statusDestino = $filter('getBy')('tasks', $scope.kanban.status, moveData.dest);
			//console.log(status);
			if(statusOrigem != null) {
				kanbanRefreshService.sendMessage({
						type: 'status',
						project_name: $scope.projectName,
						kanban_id: $scope.kanban._id,
						status_id: statusOrigem._id
				});
			}
			if(statusDestino != null) {
				kanbanRefreshService.sendMessage({
						type: 'status',
						project_name: $scope.projectName,
						kanban_id: $scope.kanban._id,
						status_id: statusDestino._id
				});
			}
		}
		//alterar todas as tasks a partir da task movida até o fim da lista alvo
		task.id = task._id;
		task.status_id = targetIdentity;
		task.position = moveData.destPosition;
		var taskData = new $scope.TaskResource(task);
		taskData.$move(enviarMsgAtualizacao);
	};
	$scope.taskEventCount = 0;
	$scope.taskEvent = function(){
		return $scope.taskEventCount;
	}

	$scope.column_size_class = function() {
		var columns_deleted = $scope.kanban.status_deleted ? $scope.kanban.status_deleted.length : 0;
		var columns_count = Math.abs($scope.kanban.status.length -  columns_deleted);
		var float_size = 12 / columns_count;
		var rounded_size = Math.round(float_size);
		return 'span' + rounded_size;
	}

	$scope.column_size_style = function(){
		var columns_deleted = $scope.kanban.status_deleted ? $scope.kanban.status_deleted.length : 0;
		var columns_count = Math.abs($scope.kanban.status.length -  columns_deleted);
		var float_size = 100 / columns_count;
		return 'width: ' + float_size + '%;';
	}

	$scope.backlogTasks = [];
	$scope.getBacklogTasks = function() {
		var statusTasksResource = webServiceStorage.StatusTasks($routeParams.project_name, $routeParams.kanban_id,$scope.getCurrentKanban().status[0]._id);
				$scope.backlogTasks = statusTasksResource.query();
	};

	$scope.getCurrentKanban = function() {
		return $scope.kanban;
	}

	$scope.getTaskFieldValue = function(task, field) {
		return 'XXXX';
		return task[field] || 'Field';
	}

	$scope.kanbanTaskFieldChanged = function(task, field) {
		var taskResource = new $scope.TaskResource(task);
		taskResource.$update(function(){
			kanbanRefreshService.sendMessage({
					type: 'task',
					project_name: $scope.projectName,
					kanban_id: task.kanban_id,
					status_id: task.status_id,
					task_id: task.id || task._id
				});
		});
	}
	$scope.hideControlEnterTip = function(column) {
		return (column.newTask.text == '');
	};
	$scope.showControlEnterTip = function(column) {
		return (column.newTask.text != '');
	};

	$scope.navegar = function(projectName, kanban_id) {
		$location.path('projeto/' + projectName + '/' + kanban_id);
		webServiceStorage.setLastKanban(null);
	};

	$scope.statusNameChanged = function(status) {
		var kanban = $scope.getCurrentKanban();
		var statusData = new $scope.StatusResource({ _id: status._id, name: status.name });
		return statusData.$rename(function(result){
			kanbanRefreshService.sendMessage({
							type: 'status_rename',
							project_name: $scope.projectName,
							kanban_id: $scope.kanban._id,
							status_id: status._id,
							status_name: status.name
					});
		});
	};

	$scope.statusEditFilter = function(status){
		if($scope.show_columns_removed){
			return true;
		}
		else {
			return $scope.isStatusNotDeleted(status);
		}
	}

	$scope.isStatusNotDeleted = function(status) {
		if($scope.getCurrentKanban().status_deleted){
			return $scope.getCurrentKanban().status_deleted.indexOf(status._id) == -1;
		}
		return true;
	}

	$scope.isStatusDeleted = function(status) {
		if($scope.getCurrentKanban().status_deleted){
			return $scope.getCurrentKanban().status_deleted.indexOf(status._id) > -1;
		}
		return false;
	}

	$scope.restoreStatusColumn = function(status){

		var kanban_config_data = {
				_id: $scope.kanban._id,
				status_deleted: status._id
			};

		var kanban = new KanbanResource(kanban_config_data);

		return kanban.$status_restore(function(data){
			$scope.getCurrentKanban().status_deleted = data.status_deleted;

			kanbanRefreshService.sendMessage({
							type: 'status_restore',
							project_name: $scope.projectName,
							kanban_id: $scope.kanban._id,
							status_id: status._id,
							status_deleted: data.status_deleted
					});
		});
	}

	$scope.deleteStatusColumn = function(status){
		var kanban_config_data = {
				_id: $scope.kanban._id,
				status_deleted: status._id
			};

		var kanban = new KanbanResource(kanban_config_data);

		return kanban.$status_delete(function(data){
			$scope.getCurrentKanban().status_deleted = data.status_deleted;
			//if($scope.getCurrentKanban().status_deleted)
			//{
			//	$scope.getCurrentKanban().status_deleted.push(status._id);
			//} else {
			//	$scope.getCurrentKanban().status_deleted = [status._id];
			//}
			kanbanRefreshService.sendMessage({
							type: 'status_deleted',
							project_name: $scope.projectName,
							kanban_id: $scope.kanban._id,
							status_id: status._id,
							status_deleted: data.status_deleted
					});
		})
	}

//inicializando varáveis
	$scope.projectName = $routeParams.project_name;

	$scope.controlEnterTipEnabled = false;

	$scope.kanban = webServiceStorage.getLastKanban(function(data){

	});

	$scope.getUploadUrl = function(){
		if($scope.kanban == null || $scope.kanban == 'undefined') {
			return '';
		}
		return '/upload?project=' + $scope.projectName +'&kanban=' + $scope.getCurrentKanban()._id;
	}

	$scope.completeUpload = function(result){
		if(result.error != null && result.error != undefined){
			$scope.errorUpload = result.error;
		} else {
			$scope.uploadObject = result;
			$scope.errorUpload = null;
		}
	}
	$scope.limparUpload = function(){
		$scope.errorUpload = null;
		$scope.uploadObject = null;
	}

	$scope.processTasksUpload = function(){
		var kanban_config_data = {
			_id: $scope.kanban._id,
			upload_id: $scope.uploadObject.id
		};

	var kanbanResource = new KanbanResource(kanban_config_data);

	return kanbanResource.$batch_load(function(data){

		kanbanRefreshService.sendMessage({
						type: 'batch_load',
						project_name: $scope.projectName,
						kanban_id: $scope.kanban._id
				});
		$scope.limparUpload();

		window.location.reload();
	});
	}
	$scope.TaskResource = webServiceStorage.Task($routeParams.project_name, $routeParams.kanban_id);

	$scope.StatusResource = webServiceStorage.Status($routeParams.project_name, $routeParams.kanban_id);

	var KanbanResource = webServiceStorage.Kanban($routeParams.project_name);

	$scope.tasks_fields_data_types = [
		{label: 'Texto', name: 'String'},
		{label: 'Data', name: 'Date'},
		{label: 'Inteiro', name: 'Integer'}
	];
  //scrum config methods
  $scope.deliveryStatusChanged = function(){
  	console.log("COLUMN ID SELECTED AS DELIVERY STATUS: ");
  	console.log($scope.kanban.scrum_config.delivery_status_id);
  }

  $scope.buildBurndownData = function(){
  	var startDate = moment($scope.kanban.scrum_config.start_date,'DD/MM/YYYY');
  	var endDate = moment($scope.kanban.scrum_config.end_date, 'DD/MM/YYYY');
  	var arrPointsDelivery = [];
		var totalPoints = 0;

		var estimation_field_name = $scope.kanban.scrum_config.task_estimation_field;

		for(var status in $scope.kanban.status){
			for(var task in $scope.kanban.status[status].tasks){
				totalPoints += parseInt($scope.kanban.status[status].tasks[task][estimation_field_name]);
			}
		}

		console.log("TOTAL POINTS: ", totalPoints );

		var diffInDays = endDate.diff(startDate, 'days') + 1;

		arrPointsDelivery.push(totalPoints);

		//for each day get how many points were missing to be delivered
		for(var i=1; i<diffInDays; i++){
			var day =  moment($scope.kanban.scrum_config.start_date,'DD/MM/YYYY').add(i, 'days');
			console.log("DAY: ", day.format('DD/MM/YYYY'));
		}
		console.log('DIFF IN DAYS');
		console.log(diffInDays);
  }

  $scope.saveScrumConfig = function() {
			var kanban_config_data = {
				_id: $scope.kanban._id,
				scrum_config: $scope.kanban.scrum_config
			};

			var kanban = new KanbanResource(kanban_config_data);

			var errorHandlerCallback = function(response){
				alert('Não foi possível salvar a configuração: \n' + response.data.message )
			}


			//persist new kanban settings
			kanban.$update_config(function(result){

				//$scope.kanban.scrum_config = $scope.scrum_config;

				//broadcast config update to all clients
				kanbanRefreshService.sendMessage({
					type: 'kanban_scrum_config',
					project_name: $scope.projectName,
					kanban_id: $scope.kanban._id,
					scrum_config: $scope.kanban.scrum_config
				});
			}, errorHandlerCallback);

		};

	function setupKanbanRefreshService() {
		//configura o kanbanRefreshService para escutar atualizações do kanban corrente
		kanbanRefreshService.setup($scope.kanban._id, function(data){
			//console.log(typeof data);
			if(typeof data === 'object'){
				if(data.kanban_id == $scope.kanban._id) {
					var status = $filter('getBy')('_id',$scope.kanban.status, data.status_id);
					if(data.type){
						switch(data.type){
							case 'status':
								status.tasks = data.tasks;
								break;
							case 'status_rename':
								status.name = data.status_name;
								break;
							case 'task':
								var task = $filter('getBy')('_id', status.tasks, data.task._id);
								var task_index = status.tasks.indexOf(task);
								status.tasks[task_index] = data.task;
								break;
							case 'kanban_config':
								$scope.kanban.tasks_fields = data.tasks_fields;
								$scope.kanban.html_template = data.html_template;
								break;
							case 'status_deleted':
								$scope.kanban.status_deleted = data.status_deleted;
								break;
							case 'status_restore':
								$scope.kanban.status_deleted = data.status_deleted;
								break;
							case 'add_status':
								if(status != null){
									$scope.kanban.status.push(data.new_status);
								}
								break;
							case 'kanban_scrum_config':
							console.log("HERE");
							console.log(data);
								$scope.kanban.scrum_config = data.scrum_config;
								break;
						}

					} else {
						var task = $filter('getBy')('_id', status.tasks, data.task._id);
						var task_index = status.indexOf(task);
						status[task_index] = data.task
					}
					$scope.$apply();
				}
			}
		});
	}


	if($rootScope.kanbans == null || $rootScope.kanbans == undefined) {
		$rootScope.kanbans = webServiceStorage.Kanban($routeParams.project_name).query();
	}

	$scope.newStatusColumnName = "";
	$scope.show_columns_removed = false;
	// $scope.removeColumn = function(index) {
	// 	if (index == 0) {
	// 		return;
	// 	}
	// 	$scope.getCurrentKanban().columns.splice(index, 1);
	// };

	$scope.addStatusColumn = function() {
			var kanban_config_data = {
				_id: $scope.kanban._id,
				new_status: $scope.newStatusColumnName
			};

		var kanban = new KanbanResource(kanban_config_data);

		return kanban.$add_status(function(data){

			$scope.newStatusColumnName = "";


			$scope.kanban.status.push(data.new_status);

			kanbanRefreshService.sendMessage({
							type: 'add_status',
							project_name: $scope.projectName,
							kanban_id: $scope.kanban._id,
							new_status: data
					});
		});
	}

	// $scope.addColumn = function() {
	// 	var new_column = $scope.newColumn($scope.newColumnName, $scope.getCurrentKanban().columns.length);
	// 	$scope.getCurrentKanban().columns.push(new_column);
	// };

	// $scope.newColumn = function(name, index) {
	// 	return {
	// 		name: name,
	// 		seq: index,
	// 		tasks: [],
	// 		newTask: {},
	// 		form_template: 'form-template2.html',
	// 		css_template: 'template2',
	// 		size: "span3"
	// 	}
	// };

/*			columns: [{
				name: 'Planejando (Backlog)',
				seq: 1,
				tasks: [],
				newTask: {},
				form_template: 'form-template1.html',
				css_template: 'template1',
				size: "span3"

			} ... */


	$scope.tasksKeypressCallback = function($event, column) {
		$scope.addTask(column);
		$event.preventDefault();
	};


	$scope.addTask = function(column) {
		$scope.taskEventCount +=1;
		var task = new $scope.TaskResource();
		task.status_id = column._id;
		task.text = column.newTask.text;
		task.$save(function(task){
			column.tasks.push(task);
			column.newTask = {
				text: ""
			};
			//console.log($scope);
			kanbanRefreshService.sendMessage({
				type: 'status',
				project_name: $scope.projectName,
				kanban_id: $scope.kanban._id,
				status_id: column._id
			});

			if($scope.gitlabProject != null){
				gitlabService.createIssue($scope.gitlabProject.id, task.text, '').
					then(function(data){
						task.external_source = 'gitlab';
						task.external_data = data;
						task.$save();
					},
					function(){
						alert('Não foi possível adicionar item ao gitlab: Projeto - ' + $scope.gitlabProject.name);
					});
			}
		});
	};

	$scope.kanbanTaskChanged = function(task_to_change, column, index) {
	//$scope.kanbanTaskChanged = function(task_to_change) {
		$scope.currentEditingTask = task_to_change;
		$scope.currentEditingColumn = column;
		$scope.currentEditingIndex = index;
		$scope.changeTaskText();
		// $scope.TaskResource.get( { taskId: task_to_change._id } , function(task){
		// 	task_to_change.text = task_to_change.text.replace(/\u00a0/g, " ");
		// 	if (task.text.trim() !== task_to_change.text.trim()){
		// 		task.text = task_to_change.text.trim();
		// 		task.text = task.text.replace("<br>", "").replace("<br/>", "").trim();
		// 		task.$save(function(){
		// 			kanbanRefreshService.sendMessage({
		// 				type: 'task',
		// 				project_name: $scope.projectName,
		// 				kanban_id: task_to_change.kanban_id,
		// 				status_id: task_to_change.status_id,
		// 				task_id: task_to_change.id
		// 			});
		// 		});
		// 	}
		// });
	};




	$scope.deleteItem = function(column, index) {
		var taskData = new $scope.TaskResource({_id: column.tasks[index]._id});
		taskData.$remove(function(){
			column.tasks.splice(index, 1);
			kanbanRefreshService.sendMessage({
				type: 'status',
				project_name: $scope.projectName,
				kanban_id: $scope.kanban._id,
				status_id: column._id
			});
		});
		$scope.taskEventCount += 1;
	};

	var currentEditingTask = {};
	$scope.setCurrentEditingTask = function(column, index) {
		var task = column.tasks[index];
		$scope.currentEditingTask = task;
		// $scope.currentEditingTask = {
		// 		id: task._id,
		// 		status_id: task.status_id,
		// 		text: task.text
		// 	};
		$scope.currentEditingColumn = column;
		$scope.currentEditingIndex = index;
	};

	$scope.getEditingTask = function() {
		if($scope.currentEditingTask != undefined)
		{
			return $scope.currentEditingTask;
		}
		else
		{
			return {text: ''};
		}
	};

	//method called when openning kanban settings modal window
	//actually does  nothing
	$scope.configurarKanban = function() {
		$scope.tasks_fields_to_edit = angular.copy($scope.kanban.tasks_fields);
		if($scope.tasks_fields_to_edit == null || $scope.tasks_fields_to_edit == 'undefined')
		{
			$scope.tasks_fields_to_edit = [{name: 'text', label: '', multiline: true}];
		}
	}

	//add new row to allow user to fill a new field to kanban tasks
	$scope.addFieldToKanbanTasks = function() {
		$scope.tasks_fields_to_edit.push({name: '', label: '', multiline: false});
	}

	//remove a kanban task field
	$scope.removeFieldFromKanbanTasks = function(indice) {
		$scope.tasks_fields_to_edit.splice(indice, 1);
	}

	$scope.getKanbanTasksFields = function(){
		return $scope.kanban.tasks_fields;
	}

	$scope.notIsTextField = function(field){
		return field.name != 'text';
	}

	var updateTask = function(task){
		var taskData = new $scope.TaskResource(task);
		taskData.collapsed = task.collapsed;
		taskData.$update(function(){
				kanbanRefreshService.sendMessage({
					type: 'task',
					project_name: $scope.projectName,
					kanban_id: task.kanban_id,
					status_id: task.status_id,
					task_id:  task.id || task._id
				});
			});
	}

	$scope.expandAllTasks = function(column) {
		for(i=0; i < column.tasks.length; i++){
			column.tasks[i].collapsed = false;
			updateTask(column.tasks[i]);
		}
		$scope.taskEventCount += 1;
	}

	$scope.collapseAllTasks = function(column) {
		for(i=0; i < column.tasks.length; i++){
			column.tasks[i].collapsed = true;
			updateTask(column.tasks[i]);
		}
		$scope.taskEventCount += 1;
	}

	$scope.toggleTaskCollapse = function(task){
		task.collapsed = !task.collapsed;
		updateTask(task);
		$scope.taskEventCount += 1;
	}

	//return kanban tasks fields
	$scope.getKanbanTasksFieldsToEdit = function() {
		return $scope.tasks_fields_to_edit;
	}

	// persist kanban settings changes
	$scope.saveKanbanConfig = function() {
		var kanban_config_data = {
				_id: $scope.kanban._id,
				tasks_fields: $scope.tasks_fields_to_edit,
				html_template: $scope.kanban.html_template
			};

		var kanban = new KanbanResource(kanban_config_data);

		var errorHandlerCallback = function(response){
			alert('Não foi possível salvar a configuração: \n' + response.data.errors )
		}


		//persist new kanban settings
		kanban.$update_config(function(result){

			$scope.kanban.tasks_fields = $scope.tasks_fields_to_edit;

			//broadcast config update to all clients
			kanbanRefreshService.sendMessage({
				type: 'kanban_config',
				project_name: $scope.projectName,
				kanban_id: $scope.kanban._id,
				tasks_fields: $scope.kanban.tasks_fields,
				html_template: $scope.kanban.html_template
			});
		}, errorHandlerCallback);
	}

	if($scope.kanban == null || $scope.kanban == undefined) {
	 	$scope.kanban = KanbanResource.get({kanbanId: $routeParams.kanban_id}, function(){
	 		setupKanbanRefreshService();
	 		// checando se kanban corrente já possui campo fields
			if($scope.kanban.tasks_fields === undefined){
				//se não possui, gera valor padrão
				$scope.kanban.tasks_fields = [{ name: 'text', label: '', multiline: true }];

				//envia dado de fields para o servidor
				$scope.saveKanbanConfig();
			}
	 	})
	}
	else {
		//console.log($scope.kanban._id);
		setupKanbanRefreshService();
		if($scope.kanban.tasks_fields === undefined){
			//se não possui, gera valor padrão
			$scope.kanban.tasks_fields = [{ name: 'text', label: '', multiline: true }];

			//envia dado de fields para o servidor
			$scope.saveKanbanConfig();
		}
	}

	$scope.changeTaskText = function() {
		var taskResource = new $scope.TaskResource($scope.currentEditingTask);
		taskResource.$update(function(){
			var task = $scope.currentEditingColumn.tasks[$scope.currentEditingIndex];
			task.text = $scope.currentEditingTask.text;
			kanbanRefreshService.sendMessage({
					type: 'task',
					project_name: $scope.projectName,
					kanban_id: task.kanban_id,
					status_id: task.status_id,
					task_id: task.id || task._id
				});
		});
	}

	$scope.color = {};

	$scope.colorDrop = {};


	$scope.dropColorCallback = function(event, ui, taskScope) {

		if(ui.draggable.hasClass('color_badge'))
		{
			taskScope.task.color = taskScope.taskColor;
			taskScope.task.id = taskScope.task._id;
			var taskData = new $scope.TaskResource(taskScope.task);
			taskData.$update(function(){
				kanbanRefreshService.sendMessage({
					type: 'task',
					project_name: $scope.projectName,
					kanban_id: taskScope.task.kanban_id,
					status_id: taskScope.task.status_id,
					task_id: taskScope.task.id
				});
			});
		}
	}

	$scope.colorDragStart = function() {

	};

	$scope.colorDragStop = function() {

	};

	$scope.colorDragCallback = function() {

	};

	$scope.gitlabProjects = [];
	$scope.gitLabIssues = [];

	//gitlabService.configure('https://gitlab.com/api/v3', 'eNmvdzADZsuErzVV7PNp');
	gitlabService.configure('https://gitlab.com/api/v3', null);

	$scope.doGitlabLogin = function(){
		gitlabService.login($scope.gitlab_login, $scope.gitlab_password).then(function(user){
			$scope.getGitLabProjectNames();
		},function(data){
			alert('Autenticação falhou: ' + data.message)
		});
	}

	$scope.getGitLabProjectNames = function(){

      gitlabService.getProjects().then(function(data){
        //$scope.gitlabProjects = data;
        $scope.gitlabProjects = [];
        for(idx in data){
          $scope.gitlabProjects.push(data[idx]);
        }
      });
	};

	$scope.gitlabProjectChanged = function(){
		gitlabService.getIssues($scope.gitlabProject.id).then(function(data){
			console.log(data);
		});
	}

	$scope.getGitLabIssues = function(){

	};
});


myApp.controller('SocketTestCtrl', function($scope, socket) {
	$scope.socket_data = [];
	socket.onconnect(function(args){
		console.log(args);
	});

	socket.ondisconnect(function(args){
		console.log(args);
	});
	$scope.enviarMensagem = function() {
		socket.emit('kanban-refresh', {kanban_id: 2, text: $scope.mensagem});
	}

	socket.on('kanban-refresh', function(){
		console.log(arguments);
		$scope.socket_data.push(arguments);
	});

	socket.emit('registerToKanbanChannel', {kanban_id: 2});

})

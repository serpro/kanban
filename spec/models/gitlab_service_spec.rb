require 'rails_helper'

describe GitlabService, :vcr => {record: :new_episodes} do

  let(:server_config) do
    {
      gitlab_server: ENV['GITLAB_TEST_SERVER'],
      private_token: ENV['GITLAB_TEST_PRIVATE_TOKEN']
    }
  end

  context 'constructor' do
    it 'needs a valid gitlab private_token' do
      expect do
        GitlabService.new(server_config)
      end.not_to raise_error
    end

    describe 'default state' do

      describe 'is_token_valid?' do
        it { expect(GitlabService.new(server_config).is_token_valid?).to eq(true) }
      end
    end

    it 'raises error if endpoint not informed' do
      expect do
        GitlabService.new({gitlab_server: '', private_token: ''})
      end.to raise_error(GitlabServiceException, GitlabService::INVALID_SERVER_OR_TOKEN)
    end
  end

  context 'service operations' do


    subject { GitlabService.new(server_config) }

    describe '#get_projects' do
      describe 'returning the projects on gitlab' do
        it { expect(subject.get_projects).not_to be_nil }
        it { expect(subject.get_projects).to be_a(Array) }
      end
    end
  end


  #ABOUT AUTO PAGINATE
  # https://github.com/NARKOZ/gitlab/pull/85
  # http://doc.gitlab.com/ce/api/ #PAGINATION

  describe '#get_project' do
    let(:gitlab_cli) { GitlabService.new(server_config) }

    subject { gitlab_cli.get_project(72959) }

    it 'return a project' do
      expect(subject).not_to be_nil
    end

  end

  describe '#get_issues' do
    let(:gitlab_cli) { GitlabService.new(server_config) }

    subject do
      gitlab_cli.get_issues(72959)
    end

    context 'returning issues from gitlab' do
      it 'shuld not be nil' do
        expect(subject).not_to be_nil
      end

      it 'return an array of issues' do
        expect(subject).to be_an(Array)
      end

      it 'return issues containing id' do
        expect(subject.first).to respond_to(:id)
      end

      it 'return issues containing internal id' do
        expect(subject.first).to respond_to(:iid)
      end

      it 'return issues containing title' do
        expect(subject.first).to respond_to(:title)
      end

      it 'return issues containing description' do
        expect(subject.first).to respond_to(:description)
      end

      it 'return issues containing author' do
        expect(subject.first).to respond_to(:author)
      end

      it 'return issues containing state' do
        expect(subject.first).to respond_to(:state)
      end
    end

    describe '#close_issue' do
      let(:gitlab_cli) { GitlabService.new(server_config) }
      let(:project_id) { 72959 }
      let(:issue_id) { 274253 }

      subject { gitlab_cli.close_issue(project_id, issue_id) }

      it 'closes a issue' do
        expect(subject.state).to eq('closed')
      end
    end

    describe '#create_issue' do
      let(:gitlab_cli) { GitlabService.new(server_config) }
      let(:issue_params) do
        {
          title: 'titulo',
          description: 'descrição'
        }
      end
      subject { gitlab_cli.create_issue(72959, issue_params)  }

      it 'returns issue created' do
        expect(subject).not_to be_nil
      end

      it 'issue created should have id' do
        expect(subject.id).not_to be_nil
      end

      it 'issue created should have title' do
        expect(subject.title).not_to be_nil
      end

      it 'issue created should have description' do
        expect(subject.description).not_to be_nil
      end
    end
  end
end

#require não necessário pois está definido no arquivo .rspec
#require './spec/spec_helper'
require 'rails_helper'
describe Project do
  before(:each) do
    User.all.destroy
    Project.all.destroy
    Project.templates.destroy
  end

  it { is_expected.to validate_uniqueness_of(:name) }
  it { is_expected.to validate_presence_of(:name) }

  it { is_expected.to embed_many :kanbans }

  it { is_expected.to belong_to :owner }

  describe 'new Project' do
    it 'is public by default' do
      expect(subject.visibility).to eq('public')
    end
  end

  describe '#public scope' do
    it 'returns only public projects' do
      public_project = Project.create(name: 'Public Project', visibility: 'public')
      private_project = Project.create(name: 'Private Project', visibility: 'private')
      expect(Project.public).to include(public_project)
      expect(Project.public).not_to include(private_project)
    end
  end

  describe '#private scope' do
    it 'returns only private projects' do
      private_project = Project.create(name: 'Private project', visibility: 'private')
      public_project  = Project.create(name: 'Public project', visibility: 'public')
      expect(Project.private).to include(private_project)
      expect(Project.private).not_to include(public_project)
    end
  end

  describe '#allow_access' do
    it 'allow any user to access public projects' do
      public_project = Project.create(name: 'Public Project')
      expect(public_project.allow_access_to(User.new)).to eq(true)
    end
    it "allow only listed users to access private projects" do
      private_project = Project.create(name: 'Private Project', visibility: 'private')
      expect(private_project.allow_access_to(User.new)).to eq(false)
      user_allowed = User.create!(name: 'User Man', login: 'user_man', email: 'user_man@server.com', uid: '1233')
      private_project.team_members << user_allowed
      expect(private_project.allow_access_to(user_allowed)).to eq(true)
    end
  end

  describe "templates" do
    before do
      Project.create!(name: "default_template", type: "template")
      Project.create!(name: "template1", type: "template")
      Project.create!(name: "template2", type: "template")
      Project.create!(name: "project1", type: "normal")
    end
    describe "#default_template" do
      it "get default template if exists" do
        expect(Project.default_template).not_to be(nil)
      end
    end
    describe "#templates" do
      it "returns only project templates" do
        Project.count.should eq(1)
        Project.templates.count.should eq(3)
      end
    end
  end

  describe "#create_kanban" do
    subject { Project.create!(name: "PROJECT for #create_kanban test") }
    let(:kanban_hash) do
      {"name" => "KANBAN for #create_kanban test"}
    end

    it { subject.create_kanban(kanban_hash).id.should_not be_nil }
  end

  describe 'gitlab configuration' do
  end

  describe "#create" do
    it "add 1 more project" do
      expect {Project.create!(name: 'one project more')}.to change{Project.count}.by(1)
    end
    it "assigns id to project created" do
      project = Project.create!(name: 'project created')
      project.id.should_not be_nil
    end
    it "not allow repeated project name" do
      Project.create!(name: "project_created")
      expect{Project.create!(name: "project_created")}.to raise_error(Mongoid::Errors::Validations)
    end
  end
end

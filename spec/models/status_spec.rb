require 'rails_helper'

describe Status do
  before do
    Project.all.destroy
  end

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to be_embedded_in(:kanban) }
  it { is_expected.to have_many(:tasks) }
  describe  ".default_status_set" do
    it "returns an array containing a default set of statuses" do
      Status.default_status_set.class.should == Array
    end

    it "is this set ['Planejando (Backlog)', 'Projetando', 'Em Desenvolvimento', 'Testando', 'Entregue']" do
      Status.default_status_set.should == ['Planejando (Backlog)', 'Projetando', 'Em Desenvolvimento', 'Testando', 'Entregue']
    end

  end
end

require 'rails_helper'

describe User, :type => :model do
    it { is_expected.to validate_uniqueness_of :login }
    it { is_expected.to validate_presence_of :login }

    it { is_expected.to have_many :uploads}

    it "stores uploads" do
      u = User.create!(:login => 'new_user', :name => "New user")

      uploaded_file = double("file")

      allow(uploaded_file).to receive(:read) { "anycontent" }
      allow(uploaded_file).to receive(:original_filename) { "tasks.csv" }
      allow(uploaded_file).to receive(:content_type) { "text/csv" }

      upload = u.store_upload({'upload' => uploaded_file})

      expect(upload).not_to be_nil
      expect(upload.filepath).to eq('public/upload/new_user_tasks.csv')
      expect(upload.filename).to eq('tasks.csv')
    end
end
